<?php require_once '/bootstrap/twig.php';
      $data = array(
        'user' => array(
          'first_name' => 'Steve',
          'last_name' => 'McDonald',
          'age' => '40',
          'language' => 'english'
        ),
        'greeting' => array(
          'english' => 'Hello',
          'spanish' => 'Holla'
        )
      );
?>
<!doctype html>
<html>
  <head>
    <meta charset=utf-8>
    <title></title>
  </head>
  <body>
  <?php  echo $twig->render( 'welcome.html', $data );  ?>
  </body>
</html>
