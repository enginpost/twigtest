<?php
  require_once '/libs/vendor/autoload.php';
  error_reporting(E_ALL);
  ini_set('display_errors', '1');
  $twig = '';

  if( isset( $_SESSION['twig'] ) ){
    $twig = $_SESSION['twig'];
  }else{
    $loader = new Twig_Loader_Filesystem('views');
    $twig = new Twig_Environment($loader, array( 'cache' => 'views/cache', 'debug' => true));
    $_SESSION['twig'] = $twig;
  }
?>
